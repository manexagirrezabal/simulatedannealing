#!/usr/bin/env python
# -*- coding: utf-8 -*- 

#import silaba
#s=silaba.silaba()
#str="udan bezala lotan nengoen\neta naute ni esnatu\neuskaldun jende jatorrarentzat\nizan nadin alaigarri\nlehen hitz hutsetan zegon bertsoa\norain egin da margarri\negia baizik ez dut esango\nbi gehi bi da apaingarri"
#str="mila bederatziehun\nta lehenengo urtian\nmaiatzaren hamalau\ngarren egunian\norioko herriko\nbarraren aurrian\nbalia agertu zan\nbeatzik aldian"
#patt="10-8-10-8-10-8-10-8"
#rpatt="0-A-0-A-0-A-0-A"
#print str
#print patt
#print rpatt
#print s.bertsoaSilabatanEbaluatu(str, patt)
#print s.ebaluatu(str, patt)

#import errima
#er=errima.errima()
#print er.ebaluatu(str, rpatt)

#import curses
#stdscr = curses.initscr()
#curses.noecho()
#curses.cbreak()
#stdscr.keypad(1)



#stdscr.keypad(0)
#curses.nocbreak()
#curses.echo()
#curses.endwin()

#import curses
#import time

#def report_progress(filename, progress):
#    """progress: 0-10"""
#    stdscr.addstr(0, 0, "Moving file: {0}".format(filename))
#    stdscr.addstr(1, 0, "Total progress: [{1:10}] {0}%".format(progress * 10, "#" * progress))
#    stdscr.addstr(2, 0, "Moving file: {0}".format(filename))
#    stdscr.addstr(3, 0, "Total progress: [{1:10}] {0}%".format(progress * 10, "#" * progress))
#    stdscr.addstr(4, 0, "Moving file: {0}".format(filename))
#    stdscr.addstr(5, 0, "Total progress: [{1:10}] {0}%".format(progress * 10, "#" * progress))
#    stdscr.addstr(6, 0, "Moving file: {0}".format(filename))
#    stdscr.addstr(7, 0, "Total progress: [{1:10}] {0}%".format(progress * 10, "#" * progress))
#    stdscr.refresh()
#
#if __name__ == "__main__":
#    stdscr = curses.initscr()
#    curses.noecho()
#    curses.cbreak()
#
#    try:
#        for i in range(10):
#            report_progress("file_{0}.txt".format(i), i+1)
#            time.sleep(0.5)
#    finally:
#        curses.echo()
#        curses.nocbreak()
#        curses.endwin()
        

import errima
import state
import antipoto
s=state.state(5)
s.initialize("kaixo etxera\nkaixo maixo\nkaixo behera\nkaixo maka\nkaixo kaka\nkaixo taka")

# er=errima.errima()
ap=antipoto.antipoto()
print ap.ebaluatu([['kaixo','etxera'],
             ['kaixo','maixo'],
             ['kaixo','behera'],
             ['kaixo','lera'],
             ['kaixo','kaka'],
             ['kaixo','taka']],
            ['0','A','0','A','0','A'])
print ap.ebaluatu(s.phrase, ['0','A','0','A','0','A'])
a=[]
a=[1.0/2**(i) for i in range(1,28)]
 
def ebaluatu(str):
    dena=[]
    for ph in str:
        for w in ph:
            dena +=list(w)
    cou = Counter(dena).most_common(1)[0][1]
    return sum(a[0:cou])

#a=[['a','a'],['niz']]
#zer = ebaluatu(a)
#print zer
#cou = Counter(zer).most_common(1)[0][1]
#print cou, sum(a[0:cou])

# import time
# import numpy as np
# import matplotlib.pyplot as plt
# 
# fig=plt.figure()
# plt.axis([0,1000,0,1])
# 
# i=0
# x=list()
# y=list()
# 
# plt.ion()
# plt.show()
# 
# while i <1000:
#     temp_y=np.random.random()
#     x.append(i)
#     y.append(temp_y)
#     plt.scatter(i,temp_y)
#     i+=1
#     plt.draw()
#     time.sleep(0.05)

# import ngramgarbi
# ngramFile="/home/manex/corpusak/denaEsaldikaLetrakSoilikLSG.probs3"
# ngramFile = "../input/arrieta.lm"
# ng = ngramgarbi.ngramgarbi(ngramFile, 3)
# print ng.phraseProb("En un amigo")
        