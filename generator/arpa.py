


def loadArpaFile(filename, zenbat):
    ema=[]
    ema.append(0)
    for i in range(1,zenbat+1):
        ema.append({})
        
    f = open(filename, 'r',encoding="iso8859")

    line = f.readline().rstrip()
    while not line == '\\1-grams:':
        line = f.readline().rstrip()
        
    for i in range(2,zenbat+1):
        line = f.readline().rstrip()
        while not line == '\\'+str(i)+'-grams:':
            if len(line) > 0:
                linev = line.split('\t');
                ema[i-1][linev[1]] = 10**float(linev[0])
            line = f.readline().rstrip()
        lasti=i
        

    line = f.readline().rstrip()
    while not line == '':
        if len(line) > 0:
            linev = line.split('\t');
            ema[lasti][linev[1]] = 10**float(linev[0])
        line = f.readline().rstrip()

    f.close()
    
    return ema
