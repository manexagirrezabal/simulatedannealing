#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import sys
import random
import operator
import arpa

import functools

class ngram(object):
    def __init__(self, filename, zenbat):
        self.ngramak=[]
        self.ngramak.append(0)
        for i in range(1,zenbat+1):
            self.ngramak.append({})
        self.LAMBDA1=0.15
        self.LAMBDA2=0.25
        self.LAMBDA3=0.60
        
        self.ngramak = arpa.loadArpaFile(filename, zenbat)

        print ("Hizkuntza-eredua kargatua!")
        print ("..........................")
        
    def mul(self, list):
        return functools.reduce (operator.mul, list, 1)
    
    def prob(self, str):
        return self.mul ([self.phraseProb(' '.join(i)) for i in str])
        
    def phraseProb(self, str):
        strv = str.split(' ')
        dena=1.0
        for i in range(0,len(strv)-2):
            try:
                pr3=self.ngramak[3][strv[i]+" "+strv[i+1]+" "+strv[i+2]]
            except KeyError:
                pr3=0.0
            try:
                pr2=self.ngramak[2][strv[i+1]+" "+strv[i+2]]
            except KeyError:
                pr2=0.0
            try:
                pr1=self.ngramak[1][strv[i+2]]
            except KeyError:
                pr1=1.0e-100 #ZENBAKI OSO OSO TXIKIA JARRI BEHAR DA
            pr=self.LAMBDA3*pr3+self.LAMBDA2*pr2+self.LAMBDA1*pr1
            dena *= pr;
        return dena


    def learnLambdaValues(self):
        l1 = 0
        l2 = 0
        l3 = 0

        n=116539 #THIS IS NOT OK!!!
        
        t1=self.ngramak[1]
        t2=self.ngramak[2]
        t3=self.ngramak[3]

        # learn landa values
        for tg in t3 :
            tgv = tg.split(' ')
            x=tgv[0]; y=tgv[1]; z=tgv[2];
            smooth=0.0000000000001
            if (t3[x+" "+y+" "+z] > 0) :
                value1 = 1.0 * (t1[z] - 1 + smooth) / (n - 1 + smooth)
                value2 = 1.0 * (t2[y+" "+z] - 1 + smooth) / (t1[y] - 1 + smooth)
                value3 = 1.0 * (t3[x+" "+y+" "+z] - 1 + smooth) / (t2[x+" "+y] - 1 + smooth)
                if (value1 >= value2 and value1 >= value2) :
                    l1 += t3[x+" "+y+" "+z]
                elif (value2 >= value1 and value2 >= value3) :
                    l2 += t3[x+" "+y+" "+z]
                elif (value3 >= value1 and value3 >= value2) :
                    l3 += t3[x+" "+y+" "+z]

                    # normalize coefficients
        l1_norm = 1.0 * l1 / (l1 + l2 + l3)
        l2_norm = 1.0 * l2 / (l1 + l2 + l3)
        l3_norm = 1.0 * l3 / (l1 + l2 + l3)
        print (l1, l2, l3)
        print (l1_norm, l2_norm, l3_norm)

    
    def randomWord (self):
        return random.choice(list(self.ngramak[1].keys()))




#print "Start!"
#k = ngram("/home/magirrezaba008/corpusak/egunkaria.lm", 3);    
#print ("Loaded!")
#hitz = k.randomWord()
#print hitz+" ",k.ngramak[1][hitz]
#k.learnLambdaValues()

