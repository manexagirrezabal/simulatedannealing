#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import random
import sys
import ngram
#import errima
#import silaba
#import alliteration
import state
import urllib
import math
import egit
import os
#import matplotlib.pyplot as plt



#~/language-technology/srilm\ 1.7.3/bin/macosx/ngram-count -text ~/corpusak/52521-0.txt -order 5 -lm grimm.lm

initialPhrase = "Goikoetxearen alargunak\npare bat bertso\nekitaldian asko hartu\netorkizuna lantzeko\nerreparaziorako garaia\nbi urte atentaturik gabe"
corpName = "grimm.lm"

rPatt = "0-A-0-A-0-A"
sPatt = "10-8-10-8-10-8"
ngramFile = os.getenv("CORPORA")+"/"+corpName

#countMax = float(sys.argv[1])
#energyMax = float(sys.argv[2])
countMax = 100000
energyMax = 3.5

ng     = ngram.ngram(ngramFile, 3)
#er     = errima.errima()
#s      = silaba.silaba()
#al     = alliteration.alliteration()
struc  = egit.egit(rPatt, sPatt)
nlines = len(struc.rhymePatt)

def ebaluatuAliterazioak (sta, ind):
    ema = al.ebaluatu(sta.phrase)
    return ema

def ebaluatuErrima(sta, ind):
    if struc.rhymePatt[sta.changedline] != '0':
        ema = er.ebaluatu(sta.phrase, struc.rhymePatt)
    else:
        ema = sta.evals[ind]
    return ema

def ebaluatuSilaba(sta, ind):
    ema = s.ebaluatu(sta.phrase, struc.syllPatt)
    return ema

def ebaluatuNgramak(sta, ind):
    ema = ng.prob(sta.phrase)
    return ema

def ebaluatuLetrak(sta, ind):
    ema = sum([word.count("r") for verse in sta.phrase for word in verse])
    lenchars = sum([len(word) for verse in sta.phrase for word in verse])
    return ema/lenchars

    
funcnames     = ["letrak","ngramak"]
evalfunctions = [ebaluatuLetrak,ebaluatuNgramak]
evalweight    = [1,1]

def CalculateEnergy (sta):
    sttemp=state.state(len(evalfunctions))
    sta.copy(sttemp,sta)
    bat=0
    for ind,p in enumerate(evalfunctions):
        ev = p(sta, ind)
        ev  *= evalweight[ind]
        sttemp.evals[ind] = ev
        bat += ev
    sttemp.energy = bat
    return sttemp
    #return sum([p(sta) for p in evalfunctions])
        
def neighbours (sta):
    retsta=state.state(len(evalfunctions))
    sta.copy(retsta, sta)
    ranli = random.randint(0, len(sta.phrase)-1) #Select a tandom line
    ranwo = random.randint(0, len(sta.phrase[ranli])-1) #Select a random word
    retsta.changedline = ranli #Save the random line position
    retsta.changedword = ranwo #Save the random word position
    word = ng.randomWord() #Change word in line ranwo
    retsta.setWord(ranli, ranwo, word)
    return retsta
    
def P (e, ne, temp):
    if (ne - e)<0:
        return 0.0
    else:
        return (ne - e)**temp
    
st = state.state(len(evalfunctions)) #STATE
ns = state.state(len(evalfunctions)) #NEW STATE
bs = state.state(len(evalfunctions)) #BEST STATE

print ("Ea ba! Let's start")
st.initialize(initialPhrase)
print ("Started with...")
print (st.getState())
print ("sentence")
st = CalculateEnergy (st)
print ("Energy caluculated!")
st.copy(bs,st)
count = 1
print ("Initial state: ")
print (st.getState())
print ("Sakatu ENTER hasteko...")
sys.stdin.readline()

#fig=plt.figure(1, figsize=(15.0, 15.0))
#plt.axis([0,50,0,500])

#stdscr = curses.initscr()
#curses.noecho()
#curses.cbreak()
#stdscr.nodelay(1)
key = 0

logMax=math.log(countMax)   #HAU ERABILTZEN DUGU ITERAZIO BAKOITZEAN LOG(2) KALKULATU BEHAR EZ IZATEKO

opti=[]
ener=[]
ngev=[]
temp=[]
bvs=[]



i=0
x=list()
y=list()

#plt.ion()
#plt.show()
ind=0

def jarraitu(c, e, t, bvd, k):
    if count >= countMax:
        return False
    elif e >= energyMax:
        return False
    elif key == 113:
        return False
#    elif t*bvd/countMax>1e-05:
#        return False
    else:
        return True

try:
    bestValueDistance=0 #HAU ITERAZIOA GERATZEKO ERABILI BEHARKO GENUKE (STOPPING RULE, ALIZAMIR)
    temperature=0
    while jarraitu(count, st.energy, temperature, bestValueDistance, key):
#        stdscr.addstr(0, 0, count.__str__()+"    SAKATU q ATERATZEKO   "+" ".join(funcnames)+"  "+" ".join(evalweight.__str__()))
#        temperature = count.__float__()/countMax.__float__()
        temploc = count.__float__()
        temperature = math.log(temploc+1.0)/logMax
        #temperature = 1 #Annealing gabe probatzeko
        ns = neighbours (st)
        ns = CalculateEnergy (ns)
        print(chr(27)+'[2j')
        print('\033c')
        print('\x1bc')
        print(count,"/",countMax)
        print ("-NEIGH-",ns.getState(),"--")
        print ("-BEST-",bs.getState(),"--")
        print ("-STATE-",st.getState(),"--")

        #ZER IRUDIKATU NAHI DUZU?
        opti.append(temperature*bestValueDistance/countMax)
        ener.append(bs.energy)
        ngev.append(bs.evals[0])
        temp.append(temperature)
        
        if (P(st.energy, ns.energy, temperature) > random.random()):
        #if (P(st.energy, ns.energy, temperature) > 0):
            st.copy(st,ns)
        if (ns.energy > bs.energy):
            st.copy(bs,ns)
            bvs.append(bestValueDistance)
            ind +=1
            #plt.scatter(ind, bestValueDistance)
            #plt.draw()
            bestValueDistance=0
        else:
            bestValueDistance+=1
        count += 1

    print ("-NEIGH-",ns.getState(),"--")
    print ("-BEST-",bs.getState(),"--")
    print ("-STATE-",st.getState(),"--")

finally:
    sys.stderr.write("\x1b[2J\x1b[H")
    print ("HASIERAKO EGOERA")
    print (initialPhrase)
    print ("EGOERA ONENA")
    print ("Oraingoz lortu dugun egoerarik onena ", count, " pauso eginda:")
    print (bs.getState())


#     plt.plot(temp)
#     plt.xlabel("Tenperatura")
#     plt.show()
 
#     plt.plot(opti)
#     plt.xlabel("Tenperatura * azkenBalioHoberenetikDistantzia / iterazioKopuruMaximoa")
#     plt.show()

#     plt.plot(ener)
#     plt.xlabel("Egoeraren energia")
#     plt.show()

#     plt.plot(bvs)
#     plt.xlabel("Egoera onenak zenbat iterazioren ondoren lortu dira?")
#     plt.show()

#     plt.plot(ngev)
#     plt.xlabel("ngramen ebaluazioak")
#     plt.show()
