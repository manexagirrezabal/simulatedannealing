#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import random
import sys
import ngram
import errima
import silaba
import alliteration
import state
import curses
import urllib
import math
import egit
import os
#import matplotlib.pyplot as plt




initialPhrase = "Goikoetxearen alargunak\npare bat bertso\nekitaldian asko hartu\netorkizuna lantzeko\nerreparaziorako garaia\nbi urte atentaturik gabe"
initialPhrase = "bularra eman eta ondoren\nhan hasi ohi zan kantuan\nnere burua erdi lotan nik\nhan jartzen nuen orduan\nNere amatxo maitagarria\negongo zera zeruan\nhaundia hazi ez banaiz ere\nahalegin egin zenduan"
initialPhrase = "batetikan korrozka\nbestetik herrena\nkaka okupatu du\nhori da txarrena\nez da pinta kabala\nhonek dakarrena\nkaltzontzilorik gabe\nohean barrena\npixkat gutxiago edan\nezazu hurrena"
initialPhrase = "sarritan izan arren\ngogoratzeko gai\nhaiek ze sufrimendu\nta haiek ze garai\nalperrik zaude Aitor\nnik ezer esan zai\ngertatu zitzaiguna\nlatza izan zen bai\neta gai hori ez dut\norain sentitu nahi"
#initialPhrase = "blablabla bla blablabla\nbla blabla blablabla\nbla blabla blablablabla\nbla blabla blablabla\nbla blablabla blablabla\nbla blabla blablabla\nblabla blabla blablabla\nblabla bla blablabla\nbla blabla blablablabla\nbla blabla blablabla"

corpName = "denaEsaldikaLetrakSoilikLSG.probs3"
corpName = "berriaEsaldikaLetrakSoilikLSG.probs3"
corpName = "egunkaria.lm"

rPatt = "0-A-0-A-0-A-0-A-0-A"
sPatt = "7-6-7-6-7-6-7-6-7-6"
ngramFile = os.getenv("CORPORA")+"/"+corpName

#countMax = float(sys.argv[1])
#energyMax = float(sys.argv[2])
countMax = 10
energyMax = 3.5

ng     = ngram.ngram(ngramFile, 3)
er     = errima.errima()
s      = silaba.silaba()
al     = alliteration.alliteration()
struc  = egit.egit(rPatt, sPatt)
nlines = len(struc.rhymePatt)

def ebaluatuNgramak(sta, ind):
    print sta.phrase
    ema = ng.prob(sta.phrase)
    return ema

def ebaluatuAliterazioak (sta, ind):
    ema = al.ebaluatu(sta.phrase)
    return ema

def ebaluatuErrima(sta, ind):
    if struc.rhymePatt[sta.changedline] != '0':
        ema = er.ebaluatu(sta.phrase, struc.rhymePatt)
    else:
        ema = sta.evals[ind]
    return ema

def ebaluatuSilaba(sta, ind):
    ema = s.ebaluatu(sta.phrase, struc.syllPatt)
    return ema
    
funcnames     = ["ngramak", "errima", "silabak"]
evalfunctions = [ebaluatuNgramak, ebaluatuErrima, ebaluatuSilaba]
evalweight    = [1,1,1]

def CalculateEnergy (sta):
    sttemp=state.state(len(evalfunctions))
    sta.copy(sttemp,sta)
    bat=0
    for ind,p in enumerate(evalfunctions):
        ev = p(sta, ind)
        print p
        ev  *= evalweight[ind]
        print ev
        sttemp.evals[ind] = ev
        bat += ev
    sttemp.energy = bat
    return sttemp
    #return sum([p(sta) for p in evalfunctions])
        
def neighbours (sta):
    retsta=state.state(len(evalfunctions))
    sta.copy(retsta, sta)
    ranli = random.randint(0, len(sta.phrase)-1) #Select a tandom line
    ranwo = random.randint(0, len(sta.phrase[ranli])-1) #Select a random word
    retsta.changedline = ranli #Save the random line position
    retsta.changedword = ranwo #Save the random word position
    word = ng.randomWord() #Change word in line ranwo
    retsta.setWord(ranli, ranwo, word)
    return retsta
    
def P (e, ne, temp):
    if (ne - e)<0:
        return 0.0
    else:
        return (ne - e)**temp
    
st = state.state(len(evalfunctions)) #STATE
ns = state.state(len(evalfunctions)) #NEW STATE
bs = state.state(len(evalfunctions)) #BEST STATE

print "Ea ba! Let's start"
st.initialize(initialPhrase)
print "Started with..."
print st.getState()
print "sentence"
st = CalculateEnergy (st)
print "Energy caluculated!"
print st.getState()
