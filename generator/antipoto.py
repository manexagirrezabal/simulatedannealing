#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import socket
import sys
import itertools
import os

class errima(object):
    def __init__(self):
        print "jauxi!!!"
        

    
    def removeNonAscii(self,s): return "".join(filter(lambda x: ord(x)<128, s))
    
    def garbi(self, str):
        str = str.replace("á", "a")
        str = str.replace("é", "e")
        str = str.replace("í", "i")
        str = str.replace("ó", "o")
        str = str.replace("ú", "u")
        str = self.removeNonAscii(str)
        return str
    
    def ebaluatu(self, str, patt):
        if len(str) == len(patt):
            words={}
            combs=[]
            for ind,el in enumerate(patt):
#                print "."
                try:
                    words[el]=words[el]+"-"+self.garbi(' '.join(str[ind]))
                except KeyError:
                    words[el]=self.garbi(' '.join(str[ind]))
            for w in words:
                if w != '0':
                    combs.extend(self.lortuKonbinazioak(words[w]))
            emaitza = sum([self.egiaztatuBikotea(c) for c in combs])/len(combs)
            return emaitza
        else:
            return 0.0
        
    def egiaztatuBikotea(self, konb):
        self.sock.sendto(konb[0]+"-"+konb[1]+"\n", (self.UDP_IP, self.UDP_PORT))
        ema,helb=self.sock.recvfrom(1024)
        ema=ema.rstrip()
        if (ema == '?+'):
            return 0.0
        else:
            return 1.0


#er = errima();

#sar = sys.stdin.readline().rstrip()
#while (sar != ''):
#    print er.ebaluatu(sar, " ")
#    sar = sys.stdin.readline().rstrip()
    