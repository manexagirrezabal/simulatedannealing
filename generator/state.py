#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import copy

class state(object):
    """
    State object
    """
    def __init__(self, nfunctions):
        """
        Constructor function, in which we initialize the state's variables:
        """
        self.changedline=-1
        """
        -changedline: What's the last line we changed? Default = -1
        """
        self.changedword=-1
        """
        -changedword: What's the last word we changed? Default = -1
        """
        self.phrase=[[]]
        """
        -phrase:      The generated verse/phrase. It is a list of lists. Each element is a string
        """
        self.energy=0.0;
        """
        -energy:      The evaluation that the phrase got
        """
        self.evals=[]
        """
        -evals:       A list containing the evaluations
        """
        for i in range(0, nfunctions):
            self.evals.append(0.0)
        
    def initialize(self, p):
        self.phrase=[i.split(" ") for i in p.split("\n")]
        
    def setWord(self, linepos, wordpos, word):
        self.phrase[linepos][wordpos] = word
        
    def setEnergy(self, en):
        self.energy=en
        
    def getState(self):
        output = ""
        output += "--------------------------------------------------------------\n"
        output += "This is the state that has the evaluations: "
        #output += ' '.join(self.evals)
        for i in self.evals:
            output += str(i)+"--"
        output += "___"+str(self.energy)
        output += "\n"
        output += "Changed line and word:"
        output += " "+str(self.changedline)
        output += " "+str(self.changedword)
        output += "\n"
        for php in self.phrase:
            output += " ".join(php)+"\n"
        output += "--------------------------------------------------------------"
        return output
            
    @staticmethod
    def copy (st1, st2):
        st1.changedline=st2.changedline
        st1.changedword=st2.changedword
        st1.phrase=copy.deepcopy(st2.phrase)
        st1.energy=st2.energy
        st1.evals=st2.evals[:]
        