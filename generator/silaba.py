#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import socket
import sys
import re
import os

class silaba(object):
    def __init__(self):
        self.UDP_IP = "127.0.0.1"
        self.UDP_PORT = int(os.getenv("SYLLPORT"))
        self.sock = socket.socket(socket.AF_INET, # Internet
                      socket.SOCK_DGRAM) # UDP
        print "UDP target IP:", self.UDP_IP
        print "UDP target port:", self.UDP_PORT
        print "Gogoan al duzu \"flookup -ibx /home/manex/fsttresnak/silaba.fst -S -A 127.0.0.1 -P 6869\" exekutatzea?"
        print ("Silaba kontatzailea martxan!")
        print "............................"
        
    def esaldiaSilabatanBanatu (self, esaldi):
        self.sock.sendto(esaldi.lower()+"\n", (self.UDP_IP, self.UDP_PORT))
        ema,helb=self.sock.recvfrom(1024)
        ema=ema.rstrip()
        silKop = ema.count(" ") + ema.count("|") + 1
        vsvflex = len(re.findall('[aeiou]\ [aeiou]', ema))
        vhvflex = ema.count("a|ha")+ema.count("e|he")+ema.count("i|hi")+ema.count("o|ho")+ema.count("u|hu")
        return (silKop-vsvflex-vhvflex), silKop
    
    def garbi (self, str):
        str = str.replace("á", "a")
        str = str.replace("é", "e")
        str = str.replace("í", "i")
        str = str.replace("ó", "o")
        str = str.replace("ú", "u")
        str = self.removeNonAscii(str)
        return str

    def bertsoaSilabatanEbaluatu(self, str, patt):
        ebal = []
        for ind,line in enumerate(str):
            min,max=self.esaldiaSilabatanBanatu(self.garbi(' '.join(line)))
#            print ' '.join(line),min,max
            if min<=int(patt[ind])<=max:
                ebal.append(1.0)
            else:
                ebal.append(0.0)
        return ebal
    
    def removeNonAscii(self,s): return "".join(filter(lambda x: ord(x)<128, s))
    
    def ebaluatu (self, str, patt):
        ebal = self.bertsoaSilabatanEbaluatu(str, patt)
        return sum(ebal)/len(ebal)
            

#er = errima();

#sar = sys.stdin.readline().rstrip()
#while (sar != ''):
#    print er.ebaluatu(sar, " ")
#    sar = sys.stdin.readline().rstrip()
    